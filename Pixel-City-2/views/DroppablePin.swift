//
//  DroppablePin.swift
//  pixel-city
//
//  Created by Nour on 10/8/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import Foundation
import MapKit
class DroppablePin : NSObject,MKAnnotation {
    dynamic var coordinate: CLLocationCoordinate2D
    var identifier: String
    
    init(coordinate: CLLocationCoordinate2D, identifier: String) {
        self.coordinate = coordinate
        self.identifier = identifier
        super.init()
    }
    
}
