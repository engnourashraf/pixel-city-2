//
//  PhotoCell.swift
//  pixel-city
//
//  Created by Nour on 10/8/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
