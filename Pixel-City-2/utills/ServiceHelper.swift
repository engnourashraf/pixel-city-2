//
//  ServiceHelper.swift
//  pixel-city
//
//  Created by Nour on 10/8/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import Foundation
import Alamofire

class ServiceHelper {
    static let instance = ServiceHelper()
    var imageUrlArray = [String]()
    var imageArray = [UIImage]()
    
    
    func FindImagesByLocation(withAnnotation annotation: DroppablePin, andNumberOfPhotos number: Int,compliation :@escaping CompilationHandler)  {
        let header = [
            "Content-Type" : "application/json; charset = utf-8"
        ]
        
        let body :[String:Any]? = nil
        Alamofire.request(flickrUrl(withAnnotation: annotation, andNumberOfPhotos: number), method: .get, parameters: body, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if response.result.error == nil {
                guard let json = response.result.value as? Dictionary<String, AnyObject> else { return }
                let photosDict = json["photos"] as! Dictionary<String, AnyObject>
                let photosDictArray = photosDict["photo"] as! [Dictionary<String, AnyObject>]
                for photo in photosDictArray {
                    let postUrl = "https://farm\(photo["farm"]!).staticflickr.com/\(photo["server"]!)/\(photo["id"]!)_\(photo["secret"]!)_h_d.jpg"
                    self.imageUrlArray.append(postUrl)
                }
                compliation(true)
            }else{
                compliation(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
    
    func clearData()  {
        imageArray.removeAll()
        imageUrlArray.removeAll()
    }
    
    func cancelAllSessions() {
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
            sessionDataTask.forEach({ $0.cancel() })
            downloadData.forEach({ $0.cancel() })
        }
    }
}
