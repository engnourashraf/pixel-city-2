//
//  constant.swift
//  pixel-city
//
//  Created by Nour on 10/8/18.
//  Copyright © 2018 Nour. All rights reserved.
//

import Foundation


typealias CompilationHandler = (_ Success : Bool) -> ()
let imageVCStoryBoardId = "imageVC"
let photoCellReuseId = "photoCell"
let pinIdentifer = "droppablePin"



let apiKey = "570fc64d92c88bf4c37dad343f6e491b"

func flickrUrl(withAnnotation annotation: DroppablePin, andNumberOfPhotos number: Int) -> String {
    return "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(apiKey)&lat=\(annotation.coordinate.latitude)&lon=\(annotation.coordinate.longitude)&radius=1&radius_units=mi&per_page=\(number)&format=json&nojsoncallback=1"
}
